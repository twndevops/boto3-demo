import boto3

# pass resource to client (ex. eks, iam)
# ec2_client = boto3.client("ec2")
ec2_client = boto3.client("ec2", region_name="us-east-2")

# Using EC2 Resource to create VPC/subnets
# overwrite region if needed
ec2_resource = boto3.resource("ec2", region_name="us-east-2")
# Vpc constructor func passes reqd id
# new_vpc = ec2_resource.Vpc("custom")
vpc_1 = ec2_resource.create_vpc(
    CidrBlock="10.0.0.0/16"
)
# use VPC resource's action to create a subnet AND assoc w/ created VPC
vpc_1_subnet_1 = vpc_1.create_subnet(CidrBlock="10.0.1.0/24")
vpc_1_subnet_2 = vpc_1.create_subnet(CidrBlock="10.0.2.0/24")
vpc_tags = vpc_1.create_tags(Tags=[
    {
        "Key": "Name",
        "Value": "boto3-vpc"
    }
])

# Using EC2 Client to create VPC/subnets
# vpc_2 = ec2_client.create_vpc("10.0.0.0/16")
# vpc_2_subnet_1 = ec2_client.create_subnet(CidrBlock="10.0.1.0/24", VpcId=new_vpc["Vpc"]["VpcId"])
# vpc_2_subnet_2 = ec2_client.create_subnet(CidrBlock="10.0.2.0/24", VpcId=new_vpc["Vpc"]["VpcId"])

all_available_vpcs = ec2_client.describe_vpcs(
    # Filters=[
    #     {
    #         "Name": "state",
    #         "Values": [
    #             "available"
    #         ]
    #     }
    # ]
)

vpcs = all_available_vpcs["Vpcs"]
for vpc in vpcs:
    print(vpc["VpcId"])
    cidr_set = vpc["CidrBlockAssociationSet"]
    for cidr in cidr_set:
        print(cidr.get("CidrBlockState"))

# all_regions = ec2_client.describe_regions()
# for region in all_regions["Regions"]:
#     print(region["RegionName"])
